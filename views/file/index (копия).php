<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
?>
<h1>Files</h1>
<ul>
<?php foreach ($files as $file): ?>
    <li>
        <?= Html::encode("{$file}") ?>:
    </li>
<?php endforeach; ?>
</ul>

# LinkPager::widget(['pagination' => $pagination])
<?= yii\grid\GridView::widget([
                    'dataProvider' => $files,
                    'columns' => [['attribute' => 'fileName',],
                    ]
                    ]); ?>
