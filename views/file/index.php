<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\UploadForm;
?>

<h1>Files</h1>

<?= yii\grid\GridView::widget([
                    'dataProvider' => $files,
                    'columns' => [
                                    ['attribute' => 'fileName',
                                    'value' => function ($data)
                                    {
                                        return $data;
                                    },],
                                    ['format' => 'raw',
                                     'value' => function ($model, $key, $index, $column)
                                                {
                                                    $icon = Html::tag('span', '', ['class' => "glyphicon glyphicon-download"]);
                                                    return Html::a(
                                                        $icon,
                                                        Url::to(['file/download', 'fileName' => $model]),
                                                                [
                                                                'id' => 'view',
                                                                'data-pjax' => true,
                                                                'acion' => Url::to(['file/download', 'fileName' => $model]),
                                                                'target' => '_blank',
                                                                ]
                                                        );
                                                }
                                    ],
                                    ['format' => 'raw',
                                     'value' => function ($model, $key, $index, $column)
                                                {
                                                    $icon = Html::tag('span', '', ['class' => "glyphicon glyphicon-trash"]);
                                                    return Html::a(
                                                        $icon,
                                                        Url::to(['file/delete', 'fileName' => $model]),
                                                                [
                                                                'id' => 'view',
                                                                'data-pjax' => true,
                                                                'acion' => Url::to([Yii::$app->getRequest()->getUrl().'"', 'fileName' => $model]),
                                                                ]
                                                        );
                                                }
                                    ],
                        ]
]); ?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'header' => 'Файлы?']]) ?>
<?= $form->field($model, 'file')->fileInput() ?>
<button>Submit</button>
<?php ActiveForm::end() ?> 


