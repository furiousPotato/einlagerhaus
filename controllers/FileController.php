<?php

namespace app\controllers;

use yii\web\Controller;
use yii\data\Pagination;
use yii\helpers\FileHelper;
use yii\data\ArrayDataProvider;
use yii\helpers\Url;
use Yii;
use yii\web\Response;
use app\models\UploadForm;
use yii\web\UploadedFile;

class FileController extends Controller
{
    public function actionIndex()
    {
        $query = FileHelper::findFiles('./files');
        $pagination = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => count($query),
            ]);
            foreach ($query as $file)
            {
                $result[] = ['fileName' => $file];
            }
            $files = new ArrayDataProvider(['allModels' => $query]);
            
            $model = new UploadForm();
        if (Yii::$app->request->isPost) 
            {
            $model->file = UploadedFile::getInstance($model, 'file');
            if ($model->file && $model->validate()) 
                {                
                $model->file->saveAs('./files/' . $model->file->baseName . '.' . $model->file->extension);
                $this->redirect("index.php?r=file%2Findex");
                }
            }
            
        return $this->render('index', [
            'files' => $files,
            'pagination' => $pagination,
            'model' => $model,
            ]);
    }
    
    public function actionDelete($fileName)
    {
        FileHelper::unlink($fileName);
        $this->redirect("index.php?r=file%2Findex");
    }
    
    public function actionDownload($fileName)
    {
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename='.basename($fileName));
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($fileName));
        
        readFile($fileName);
        return $this;
    }
}
